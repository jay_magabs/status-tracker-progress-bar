import * as React from 'react';
import './fabric.scss';
import { sp } from "@pnp/sp/presets/all";
import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";

const StatusTrackerStepProgressBar = () => {

    const [isQualifyStageEnabled, setQualifyStage] = React.useState(false);
    const [isEstimateStageEnabled, setEstimateStage] = React.useState(false);
    const [isQuoteStageEnabled, setQuoteStage] = React.useState(false);
    const [isNegotiateStageEnabled, setNegotiateStage] = React.useState(false);
    const [isClosedWonStageEnabled, setClosedWonStage] = React.useState(false);
    const [isClosedLostStageEnabled, setClosedLostStage] = React.useState(false);
    const [updatedField, setUpdateField] = React.useState({});
    const [ID, setID] = React.useState(Number);

    React.useEffect(() => {
        idGetter();
    }, []);

    const idGetter = async () => {
        const getID = window.location.toString().split('=')[1];
        const id = Number(getID);
        console.log(id);
        setID(id);
        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(id).get();

        if (stage["Stage"] === "Qualify") {
            setQualifyStage(!isQualifyStageEnabled);
        }
        else if (stage["Stage"] === "Estimate") {
            setQualifyStage(!isQualifyStageEnabled);
            setEstimateStage(!isEstimateStageEnabled);

        } else if (stage["Stage"] === "Quote") {
            setQualifyStage(!isQualifyStageEnabled);
            setEstimateStage(!isEstimateStageEnabled);
            setQuoteStage(!isQuoteStageEnabled);

        } else if (stage["Stage"] === "Negotiate") {
            setQualifyStage(!isQualifyStageEnabled);
            setEstimateStage(!isEstimateStageEnabled);
            setQuoteStage(!isQuoteStageEnabled);
            setNegotiateStage(!isNegotiateStageEnabled);

        } else if (stage["Stage"] === "Closed Won") {
            setQualifyStage(!isQualifyStageEnabled);
            setEstimateStage(!isEstimateStageEnabled);
            setQuoteStage(!isQuoteStageEnabled);
            setNegotiateStage(!isNegotiateStageEnabled);
            setClosedWonStage(!isClosedWonStageEnabled);

        } else if (stage["Stage"] === "Closed Lost") {
            setQualifyStage(!isQualifyStageEnabled);
            setEstimateStage(!isEstimateStageEnabled);
            setQuoteStage(!isQuoteStageEnabled);
            setNegotiateStage(!isNegotiateStageEnabled);
            setClosedWonStage(!isClosedWonStageEnabled);
            setClosedLostStage(!isClosedLostStageEnabled);
        } else {
            setQualifyStage(!isQualifyStageEnabled);
            setEstimateStage(isEstimateStageEnabled);
            setQuoteStage(isQuoteStageEnabled);
            setNegotiateStage(isNegotiateStageEnabled);
            setClosedWonStage(isClosedWonStageEnabled);   
            setClosedLostStage(isClosedLostStageEnabled);
        }
    };

    const qualifyStage = async () => {
        
        setQualifyStage(!isQualifyStageEnabled);
        
        sp.web.lists.getByTitle("Opportunities").items.getById(ID).update({

            Stage: updatedField["Stage"]

        })

        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(ID).get();
        if (stage["Stage"] === "Qualify") {
            location.reload();
        }
       
    };

    const estimateStage = async () => {
 
        if(isEstimateStageEnabled){
            
            await sp.web.lists.getByTitle("Opportunities").items.getById(ID).update({

                Stage: updatedField["Stage"]
               
            }).then(result => {
                idGetter()
                // window.location.reload(true);
                console.log(JSON.stringify(result));
            });
          
            setClosedLostStage(isClosedLostStageEnabled);
            setClosedWonStage(!isClosedWonStageEnabled);
            setNegotiateStage(!isNegotiateStageEnabled);
            setQuoteStage(!isQuoteStageEnabled);
            setQualifyStage(isQualifyStageEnabled);

        }

        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(ID).get();
        console.log(stage);
        if (stage["Stage"] === "Estimate") {
            location.reload();
        }
        location.reload();
    };

    const quoteStage = async () => {
        setQuoteStage(!isQuoteStageEnabled);
        setEstimateStage(!isEstimateStageEnabled);
        setQuoteStage(!isQuoteStageEnabled);
        
        sp.web.lists.getByTitle("Opportunities").items.getById(ID).update({

            Stage: updatedField["Stage"]

        })

        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(ID).get();
        if (stage["Stage"] === "Quote") {
            location.reload();
        }
    };

    const negotiateStage = async () => {
        setNegotiateStage(!isNegotiateStageEnabled);
        setQuoteStage(!isQuoteStageEnabled);
        setEstimateStage(!isEstimateStageEnabled);
        
        sp.web.lists.getByTitle("Opportunities").items.getById(ID).update({

            Stage: updatedField["Stage"]

        })

        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(ID).get();
        if (stage["Stage"] === "Negotiate") {
            location.reload();
        }
    };

    const closedWonStage = async () => {
        setClosedWonStage(!isClosedWonStageEnabled);
        setNegotiateStage(!isNegotiateStageEnabled);
        setQuoteStage(!isQuoteStageEnabled);
        setEstimateStage(!isEstimateStageEnabled);
        
        sp.web.lists.getByTitle("Opportunities").items.getById(ID).update({

            Stage: updatedField["Stage"]

        })

        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(ID).get();
        if (stage["Stage"] === "Closed Won") {
            location.reload();
        }
    };

    const closedLostStage = async () => {
        setClosedLostStage(!isClosedLostStageEnabled);
        setClosedWonStage(!isClosedWonStageEnabled);
        setNegotiateStage(!isNegotiateStageEnabled);
        setQuoteStage(!isQuoteStageEnabled);
        setEstimateStage(!isEstimateStageEnabled);
        
        sp.web.lists.getByTitle("Opportunities").items.getById(ID).update({

            Stage: updatedField["Stage"]

        })

        const stage = await sp.web.lists.getByTitle("Opportunities").items.select('Stage').getById(ID).get();
        if (stage["Stage"] === "Closed Lost") {
            location.reload();
        }
    };

    return (
        <div>
            <span className="sample">
                <ul className="progressBar">
                    <li className={isQualifyStageEnabled ? "enabled" : "disabled"} 
                        onClick={() => { qualifyStage(); } } >Qualify</li>
                    
                    <li className={isEstimateStageEnabled ? "enabled" : "disabled"} 
                        onClick={() => { estimateStage();  setUpdateField({ ...updatedField, "Stage": "Estimate" })}} >Estimate</li>

                    <li className={isQuoteStageEnabled ? "enabled" : "disabled"} 
                        onClick={() => {setUpdateField({ ...updatedField, "Stage": "Quote" }); quoteStage(); } } >Quote</li>

                    <li className={isNegotiateStageEnabled ? "enabled" : "disabled"} 
                        onClick={() => { negotiateStage(); setUpdateField({ ...updatedField, "Stage": "Negotiate" }) } }>Negotiate</li>

                    <li className={isClosedWonStageEnabled ? "enabled" : "disabled"} 
                        onClick={() => { closedWonStage(); setUpdateField({ ...updatedField, "Stage": "Closed Won" }) }} >Closed Won</li>

                    <li className={isClosedLostStageEnabled ? "enabled" : "disabled"} 
                        onClick={() => { closedLostStage(); setUpdateField({ ...updatedField, "Stage": "Closed Lost" }) } } >Closed Lost</li>
                </ul>
            </span>
        </div>
    );
};
export default StatusTrackerStepProgressBar;