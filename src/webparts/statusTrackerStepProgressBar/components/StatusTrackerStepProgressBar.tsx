import * as React from 'react';
import styles from './StatusTrackerStepProgressBar.module.scss';
// import { IStatusTrackerStepProgressBarProps } from './IStatusTrackerStepProgressBarProps';
// import { escape } from '@microsoft/sp-lodash-subset';
import StatusTrackerStepProgressBar from './stepProgressBar-Code';

const stepProgressBar = () => {
  return (
    <div>
      <div className="container">
        <StatusTrackerStepProgressBar/>
      </div>
    </div>
  );
};
export default stepProgressBar;

// export default class StatusTrackerStepProgressBar extends React.Component<IStatusTrackerStepProgressBarProps, {}> {
//   public render(): React.ReactElement<IStatusTrackerStepProgressBarProps> {
//     return (
//       <div className={ styles.statusTrackerStepProgressBar }>
//         <div className={ styles.container }>
//           <div className={ styles.row }>
//             <div className={ styles.column }>
//               <span className={ styles.title }>Welcome to SharePoint!</span>
//               <p className={ styles.subTitle }>Customize SharePoint experiences using Web Parts.</p>
//               <p className={ styles.description }>{escape(this.props.description)}</p>
//               <a href="https://aka.ms/spfx" className={ styles.button }>
//                 <span className={ styles.label }>Learn more</span>
//               </a>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
