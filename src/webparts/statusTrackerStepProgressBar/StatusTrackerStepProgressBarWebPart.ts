import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { sp } from "@pnp/sp/presets/all";
import * as strings from 'StatusTrackerStepProgressBarWebPartStrings';
import StatusTrackerStepProgressBar from './components/StatusTrackerStepProgressBar';
import { IStatusTrackerStepProgressBarProps } from './components/IStatusTrackerStepProgressBarProps';

export interface IStatusTrackerStepProgressBarWebPartProps {
  description: string;
}

export default class StatusTrackerStepProgressBarWebPart extends BaseClientSideWebPart <IStatusTrackerStepProgressBarWebPartProps> {

  public onInit(): Promise<void> { return super.onInit().then(_ => { sp.setup({ spfxContext: this.context }); }); }

  public render(): void {
    const element: React.ReactElement<IStatusTrackerStepProgressBarProps> = React.createElement(
      StatusTrackerStepProgressBar,
      {
        description: this.properties.description,
        context: this.context
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
