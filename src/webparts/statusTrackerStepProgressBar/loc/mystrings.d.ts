declare interface IStatusTrackerStepProgressBarWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'StatusTrackerStepProgressBarWebPartStrings' {
  const strings: IStatusTrackerStepProgressBarWebPartStrings;
  export = strings;
}
